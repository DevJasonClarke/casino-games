import React, { useContext, useEffect, useState } from 'react';
import 'firebase/auth'
import { auth } from '../firebase';

const AuthContext = React.createContext();

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({ children }) {

    const [currentUser, setCurrentUser] = useState()

    function login(email, password){
       return auth.signInWithEmailAndPassword(email, password)
    }

    const signUp = (email, password) => {
        return auth.createUserWithEmailAndPassword(email, password)
    }

    const logOut = () => {
        return auth.signOut();
    }

    useEffect(() => {
         const unsubscribe = auth.onAuthStateChanged(user => {
            setCurrentUser(user)
        })
        
        return unsubscribe
    }, [])

    
    const value = {
        currentUser,
        logOut,
        signUp,
        login
    }

    return (
        <AuthContext.Provider value={value} >
            {children}
        </AuthContext.Provider>
    )
}
