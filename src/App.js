import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import "./css/style.css";
import Nav from "./components/nav";
import About from "./components/about";
import Home from "./components/home";
import Play from "./components/play";
import Profile from "./components/Profile";
import Login from "./components/login";
import { AuthProvider } from "./Context/AuthContext";
import PrivateRoute from './components/PrivateRoute';

// import { useAuth } from "./Context/AuthContext";

function App() {

  // const { currentUser } = useAuth();
  

  return (
   
      <main>
        <Nav />
          <Router>
            <AuthProvider>
              <Switch>
                <PrivateRoute exact path="/profile" component={Profile} />
                <PrivateRoute exact path="/about" component={About} />
                <PrivateRoute exact path="/play" component={Play} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/" component={Home} />
              </Switch>
            </AuthProvider>
          </Router>
      </main>
    
  );
}

export default App;
