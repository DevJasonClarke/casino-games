import firebase from 'firebase/app';
import 'firebase/auth';

const app = firebase.initializeApp({
    apiKey: "AIzaSyC4O6e1WicpmQhLR0-OQnPg16XqJWoVdlo",
    authDomain: "casino-project-a6f43.firebaseapp.com",
    projectId: "casino-project-a6f43",
    storageBucket: "casino-project-a6f43.appspot.com",
    messagingSenderId: "657138415624",
    appId: "1:657138415624:web:c37adba7c296f95add39bd",
    measurementId: "G-FLELLL0ZRL"
})

export const auth = app.auth();
export default app;