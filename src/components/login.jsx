import { useState } from "react";
import { useHistory } from "react-router";
import { useAuth } from "../Context/AuthContext";
import "../css/login.css"
function About() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState('');
  const { login, currentUser } = useAuth();
  const [loading, setLoading] = useState(false)
  let history = useHistory();

  const handleLogin = async () => {
    // e.prevent.default();

    try{
      setError("")
      await login(email, password)
      history.push('/profile')
    }catch{
      setError('Failed to login')
    }
    
    // console.log(currentUser.email)
  }

    return (
      <div className="login">
      <div className="input-container">
     <div className="input">
       
                  <input
                    className="input-text"
                    name="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    id="parentNumber"
                    placeholder="email@domain.com"
                    title="Email Address"
                
                  />

                  <label className="input-label">Email Address</label>
                </div>
                <div className="input">
                  <input
                    className="input-text"
                    name="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    id="parentNumber"
                    type='password'
                    placeholder="********"
                    title="Password"
                
                  />

                  <label className="input-label">Password</label>
                </div>
                <button onClick={handleLogin} className="button">
                  Login
                </button>
       </div>
       </div>
    );
  }
  
  export default About;
  