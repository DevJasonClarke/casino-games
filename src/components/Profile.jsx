import React from 'react';
import '../css/Profile.css';
import { useHistory } from "react-router";
import { useAuth } from "../Context/AuthContext";

function Profile() {

    const { currentUser, logOut } = useAuth();
    let history = useHistory();

    const handleLogOut = async () => {
        //setError('') - Suniel, remember to set this state.

        try{
            await logOut()
            history.push('/login')
        }catch{
            //setError - Suniel, remember to set this state.
        }
    }

    return (
        <div className="profile">
            <div className="profile-name-cont">
                <h2 className="profile-title" >Profile</h2>
                <p className="profile-name">Email: {currentUser.email}</p>
                <button onClick={handleLogOut} className="logout-button" >Log out</button>
            </div>
        </div>
    )
}

export default Profile
